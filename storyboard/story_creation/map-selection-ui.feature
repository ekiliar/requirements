Feature: Change Map selection UI
    In order to have better visibility on selection of map 
    As a User
    I want feature to change UI for map selection

    Scenario: 'Select Map' and 'Re-order photos' buttons are shown
        Given User started to create a Story
        And User selected photos for the Story
        And User provided caption and description and cliked Next
        When Map selection screen is opened
        Then Screen has two buttons 'Select Map' and 'Re-order photos'
        And both buttons grey by default
        And If selected they are pink and will show the respective view
        And if the other button is selected it will animate to the other size with the other view
    
    Background: Map selection or photo order will close
        Given User on the Map Selection Screen

    Scenario: Map is moved 
        When User moves the map
        Then Map selection or photo order closes
    
    Scenario: User searches 
        When User start searches
        Then Map selection or photo order closes

    Scenario: User selects the current selected button 
        When User clicks on the buttons that is currently selected
        Then Map selection or photo order closes

    
    
    